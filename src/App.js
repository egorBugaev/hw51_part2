import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const Header = props => {

    return (
            <header className="header">
                <div className="all">
                    <div className=" logo header__logo">
                        <a href="#" className="logo__link"><span className="logo__link--texttrans">Modus</span> versus</a>
                    </div>
                    <div className=" nav header__nav">
                        <nav className="nav__links">
                            <ul className="nav__links-list">
                                <li className="nav__link-list"><a className="nav__link" href="#">Home</a></li>
                                <li className="nav__link-list"><a className="nav__link" href="#">About</a></li>
                                <li className="nav__link-list"><a className="nav__link" href="#">Services</a></li>
                                <li className="nav__link-list"><a className="nav__link" href="#">Blog</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>

    );
}
const Promo = props => {

        return (
<div className="promo">
    <div className="all">
        <div className="promo__txt">
            <h3 className="promo__title">Vestibulum</h3>
            <p className="promo__text">Maecenas tincidunt, augue et rutrum condimentum, libero lectus mattis orci, ut commodo.</p>
        </div>
    </div>
</div>
        );
}

const Services = props =>{
            return (
            <div className="services">
                <div className="all">
                    <div className="services__title-block">
                        <div className="services__title-block-wrap">
                            <h4 className="services__title-block-title">Some of our top services</h4>
                            <p className="services__title-block-text">Ut eleifend libero sed neque rhoncus consequat. Maecenas tincidunt, augue et rutrum condimentum, libero lectus mattis orci, ut commodo. </p>
                        </div>
                        <a href="#" className="services__title-block-link">view more</a>
                    </div>
                    <div className="services__cards">
                        <div className="services__card">
                            <a href="#" className="services__card-link-ico"><i className="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
                            <h5 className="services__card-title">Suspendise</h5>
                            <p className="services__card-text"> Quisque id tellus quis risus vehicula vehicula ut turpis. In eros nulla, placerat vitae at, vehicula ut nunc. </p>
                            <a href="#" className="services__card-link-btn">Read More</a>
                        </div>
                        <div className="services__card">
                            <a href="#" className="services__card-link-ico"><i className="fa fa-key" aria-hidden="true"></i></a>
                            <h5 className="services__card-title">Maecenas</h5>
                            <p className="services__card-text">Ut eleifend libero sed neque rhoncus consequat. Maecenas tincidunt, augue et rutrum condimentum, libero lectus mattis orci, ut commodo.</p>
                            <a href="#" className="services__card-link-btn">Read More</a>
                        </div>
                        <div className="services__card">
                            <a href="#" className="services__card-link-ico"><i className="fa fa-flag" aria-hidden="true"></i></a>
                            <h5 className="services__card-title">Aliquam</h5>
                            <p className="services__card-text">Vivamus eget ante bibendum arcu vehicula ultricies. Integer venenatis mattis nisl, vitae pulvinar dui tempor non. </p>
                            <a href="#" className="services__card-link-btn">Read More</a>
                        </div>
                        <div className="services__card">
                            <a href="#" className="services__card-link-ico"><i className="fa fa-flask" aria-hidden="true"></i></a>
                            <h5 className="services__card-title">Habitasse</h5>
                            <p className="services__card-text">Astehicula ultricies. Integer venenatis mattis nisl, vitae pulvinar dui tempor non. </p>
                            <a href="#" className="services__card-link-btn">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        );

}
const About = props =>{
        return (
            <div className="about">
                <div className="all">
                    <div className="about__title">
                        <h5 className="about__title-title">Why Modus Versus?</h5>
                        <p className="about__title-text">Capacitance cascading integer reflective interface data development high bus cache dithering transponder.</p>
                    </div>
                    <div className="about__cards">
                        <div className="about__card">
                            <h5 className="about__card-title">Why Choose Us?</h5>
                            <ul className="about__card-list-wrap">
                                <li className="about__card-list">
                                    <p className="about_card-list-text">Quisque at massa ipsum</p>
                                </li>
                                <li className="about__card-list">
                                    <p className="about_card-list-text">Maecenas a lorem augue, egestas</p>
                                </li>
                                <li className="about__card-list">
                                    <p className="about_card-list-text">Cras vitae felis at lacus eleifend</p>
                                </li>
                                <li className="about__card-list">
                                    <p className="about_card-list-text">Etiam auctor diam pellentesque</p>
                                </li>
                                <li className="about__card-list">
                                    <p className="about_card-list-text">Nulla ac massa at dolor</p>
                                </li>
                                <li className="about__card-list">
                                    <p className="about_card-list-text">Condimentum eleifend vitae vitae</p>
                                </li>
                            </ul>
                        </div>
                        <div className="about__card about__card--x2">
                            <h5 className="about__card-title">About the project</h5>
                            <p className="about__card-text">Curabitur quis nisl in leo euismod venenatis eu in diam. Etiam auctor diam pellentesque lectus <a href="#" class="about__card-text-link">vehicula mattis</a>. Nulla ac massa at dolor condimentum eleifend vitae vitae urna.
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui omnis vitae enim rerum similique consequuntur corporis molestias, error neque consequatur sed odio illum non expedita distinctio quod consectetur illo optio.
                            </p>
                        </div>
                        <div className="about__card">
                            <h5 className="about__card-title">What Client’s Say?</h5>
                            <p  className="about__card-text about__card-text--bg">Curabitur quis nisl in leo euismod venenatis eu in diam. Etiam auctor diam pellentesque lectus vehicula mattis. Nulla ac massa at dolor condimentum</p>
                            <p className="about__card-name">Jhon Doe</p>
                        </div>
                    </div>
                </div>
            </div>
        )

}
const Footer = props =>{

        return (
            <footer className="footer">
                <div className="all">
                    <div className="footer__cards">
                        <div className="footer_card">
                            <div className=" logo footer__logo">
                                <a href="#" className="logo__link"><span className="logo__link--texttrans">Modus</span> versus</a>
                            </div>
                            <p className="footer__card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec .</p>
                        </div>
                        <div className="footer_card">
                            <h4 className="footer__card-contacts">Contacts</h4>
                            <p className="footer__card-tel">Phone:<a href="tel:182 2569 5896" class="footer__card-link">182 2569 5896</a></p>
                            <p className="footer__card-mail">e-mail:<a href="mailto:info@modu.versus" class="footer__card-link">info@modu.versus</a></p>
                        </div>
                        <div className="footer_card">
                            <h4 className="footer__card-blog">from the<span className="footer__card-blog-mod"> Blog</span></h4>
                            <img src="footer_blog-pic.jpg" alt="blog_pic" className="footer__card-blog-img" width="69" height="70"/>
                                <p className="footer__card-blog-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                <p className="footer__card-blog-date">26 May, 2013</p>
                        </div>
                    </div>
                </div>
            </footer>
        )

}
const Sub_footer = props => {
      return (
        <div className="sub-footer">
        <div className="all">
        <p className="sub-footer__text">2013  ModusVersus</p>
        </div>
        </div>
        )

}

const App = props => {
    return (
        <div>
            <Header/>
            <Promo/>
            <Services/>
            <About/>
            <Footer/>
            <Sub_footer/>
        </div>
    )
}



export default App;
